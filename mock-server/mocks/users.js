module.exports = [
    {
        name: 'Susan',
        id: 1,
        image: 'avatar_1.avif',
        age: 24
    },
    {
        name: 'Jenny',
        id: 2,
        image: 'avatar_2.avif',
        age: 23
    },
    {
        name: 'Marie',
        id: 3,
        image: 'avatar_3.avif',
        age: 25
    },
    {
        name: 'Cristine',
        id: 4,
        image: 'avatar_4.avif',
        age: 22
    },
    {
        name: 'Jacqueline',
        id: 5,
        image: 'avatar_5.avif',
        age: 25
    }
]
