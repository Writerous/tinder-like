const express = require('express');
const app = express();
const users = require('./mocks/users');
const reactions = require('./mocks/reactions');
const path = require('path');

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,Authorization');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

app.use(express.static(__dirname + '/static'));
app.use('/coverage', express.static(path.join(__dirname, '..', 'coverage/tinder-like')));

app.get('/jwt', (req, res) => {
    res.send({token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c'});
});

app.get('/users', (req, res) => {
    res.send(users);
});

app.post('/reaction', (req, res) => {
    const reaction = reactions.find(r => r.id === Number.parseInt(req.body?.id, 10));
    const { likes } = reaction ?? { likes: undefined };

   res.send({
       likes
   })
})

app.listen(5000, function() {
    console.log('Mock server listening on port 5000!');
});
