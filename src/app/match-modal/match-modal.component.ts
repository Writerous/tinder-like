import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
    selector: 'tin-match-modal',
    templateUrl: './match-modal.component.html',
    styleUrls: ['./match-modal.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class MatchModalComponent {

    constructor() { }

}
