import { of } from 'rxjs';

export class LoginServiceMock {

    constructor() { }

    jwtRequest$ = of('');

    jwt$ = of('');

    get jwt(): string | null {
        return '';
    }
}
