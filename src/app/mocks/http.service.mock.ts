import { Observable, of } from 'rxjs';
import { User } from '../services/user/user.service';
import { ReactionResponse, Reaction } from '../services/reaction/reaction.service';
import { Config } from '../services/config/config.service';

export class HttpServiceMock {

    constructor() { }

    getUsers(): Observable<User[]> {
        return of([]);
    }

    postReaction(reaction: Reaction): Observable<ReactionResponse> {
        return of({likes: undefined});
    }

    getJWT(): Observable<{ token: string }> {
        return of({token: ''});
    }

    getConfig(): Observable<Config> {
        return of({url: '.'});
    }
}
