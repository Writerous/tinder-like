import { Observable, of } from 'rxjs';

export class ReactionServiceMock {

    constructor() {}

    react(id: number, likes: boolean): Observable<null> {
        return of(null);
    }
}
