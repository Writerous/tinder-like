import { OnDestroy } from '@angular/core';
import { Observable, of } from 'rxjs';
import { User } from '../services/user/user.service';

export class UserServiceMock implements OnDestroy {

    constructor() {}

    users$ = of([]);

    user$: Observable<User | null> = of(null);

    nextUser(): void {}

    ngOnDestroy() {}
}
