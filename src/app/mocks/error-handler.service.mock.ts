import { ErrorHandler } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';

export class ErrorHandlerServiceMock implements ErrorHandler {
    constructor() {}

    handleError(error: Error | HttpErrorResponse) {
    }
}
