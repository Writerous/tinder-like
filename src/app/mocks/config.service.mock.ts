import { Observable, of } from 'rxjs';
import { Config } from '../services/config/config.service';

export class ConfigServiceMock {

    constructor() { }

    config: Config = {url: '.'};
    jwt: string = '';

    config$: Observable<Config> = of({url: '.'});
}
