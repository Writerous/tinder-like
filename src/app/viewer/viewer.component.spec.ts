import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';

import { ViewerComponent } from './viewer.component';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { ReactionService } from '../services/reaction/reaction.service';
import { ReactionServiceMock } from '../mocks/reaction.service.mock';
import { UserService } from '../services/user/user.service';
import { UserServiceMock } from '../mocks/user.service.mock';
import { cold, getTestScheduler } from 'jasmine-marbles';

describe('ViewerComponent', () => {
    let component: ViewerComponent;
    let fixture: ComponentFixture<ViewerComponent>;
    let userService: UserService;
    let reactionService: ReactionService;

    const init = () => {
        fixture = TestBed.createComponent(ViewerComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    }

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [
                MatButtonModule,
                MatIconModule,
            ],
            declarations: [ViewerComponent],
            providers: [
                {
                    provide: ReactionService,
                    useClass: ReactionServiceMock
                }
            ]
        })
            .overrideProvider(UserService, { useValue: new UserServiceMock() })
            .compileComponents();
    });

    beforeEach(() => {
        userService = TestBed.inject(UserService);
        reactionService = TestBed.inject(ReactionService);
    });

    it('should create', () => {
        init();
        expect(component).toBeTruthy();
    });

    it('should exec react and nextUser when like is clicked',  () => {
        init();
        spyOn(userService, 'nextUser');
        spyOn(reactionService, 'react').and.returnValue(cold('x|', {x: null}));
        component.like({id: 1, image: '', age: 24, name: 'Cathrine'});

        getTestScheduler().flush();

        expect(reactionService.react).toHaveBeenCalledOnceWith(1, true);
        expect(userService.nextUser).toHaveBeenCalledOnceWith();
    });

    it('should exec react and nextUser when dislike is clicked',  () => {
        init();
        spyOn(userService, 'nextUser');
        spyOn(reactionService, 'react').and.returnValue(cold('x|', {x: null}));
        component.dislike({id: 1, image: '', age: 24, name: 'Cathrine'});

        getTestScheduler().flush();

        expect(reactionService.react).toHaveBeenCalledOnceWith(1, false);
        expect(userService.nextUser).toHaveBeenCalledOnceWith();
    });

    it('should not exec nextUser (like is clicked) when component destroyed',  () => {
        init();
        spyOn(userService, 'nextUser');
        spyOn(reactionService, 'react').and.returnValue(cold('x|', {x: null}));
        component.like({id: 1, image: '', age: 24, name: 'Cathrine'});

        fixture.destroy();
        getTestScheduler().flush();

        expect(userService.nextUser).not.toHaveBeenCalled()
    });

    it('should not exec nextUser (dislike is clicked) when component destroyed',  () => {
        init();
        spyOn(userService, 'nextUser');
        spyOn(reactionService, 'react').and.returnValue(cold('x|', {x: null}));
        component.dislike({id: 1, image: '', age: 24, name: 'Cathrine'});

        fixture.destroy();
        getTestScheduler().flush();

        expect(userService.nextUser).not.toHaveBeenCalled()
    });

    it('should get and filter users from user service', () => {
        const user = {id: 1, image: '', age: 24, name: 'Cathrine'}
        userService.user$ = cold('ab|', { a: user, b: null });
        init();
        expect(component.user$).toBeObservable(cold('a-|', { a: user }));
    })
});
