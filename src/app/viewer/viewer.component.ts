import { ChangeDetectionStrategy, Component, OnDestroy } from '@angular/core';
import { User, UserService } from '../services/user/user.service';
import { filter, Subject, takeUntil, tap } from 'rxjs';
import { ReactionService } from '../services/reaction/reaction.service';

@Component({
    selector: 'tin-viewer',
    templateUrl: './viewer.component.html',
    styleUrls: ['./viewer.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [UserService]
})
export class ViewerComponent implements OnDestroy {

    user$ = this.userService.user$.pipe(filter(Boolean));

    imageHovered = false;

    private destroy$ = new Subject<null>();

    constructor(private userService: UserService, private reactionService: ReactionService) { }

    like(user: User) {
        this.reactionService.react(user.id, true).pipe(
            tap(() => {
                this.userService.nextUser();
            }),
            takeUntil(this.destroy$)
        ).subscribe();
    }

    dislike(user: User) {
        this.reactionService.react(user.id, false).pipe(
            tap(() => {
                this.userService.nextUser();
            }),
            takeUntil(this.destroy$)
        ).subscribe();
    }

    ngOnDestroy() {
        this.destroy$.next(null);
        this.destroy$.complete();
    }
}
