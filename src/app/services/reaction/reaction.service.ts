import { Injectable } from '@angular/core';
import { concatMap, map, Observable, of } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { MatchModalComponent } from '../../match-modal/match-modal.component';
import { HttpService } from '../http/http.service';

export interface Reaction {
    id: number;
    likes: boolean;
}

export interface ReactionResponse {
    likes?: boolean;
}

@Injectable({
    providedIn: 'root'
})
export class ReactionService {

    constructor(private httpService: HttpService, private dialog: MatDialog) {}

    react(id: number, likes: boolean): Observable<null> {
        const reaction: Reaction = { id, likes };

        return this.httpService.postReaction(reaction).pipe(
            concatMap(reactionResponse => {
                if (reaction.likes && reactionResponse.likes) {
                    return this.dialog.open(MatchModalComponent).afterClosed().pipe(map(() => null))
                }

                return of(null);
            })
        )
    }
}
