import { TestBed } from '@angular/core/testing';

import { Reaction, ReactionResponse, ReactionService } from './reaction.service';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { forkJoin, of } from 'rxjs';
import { HttpService } from '../http/http.service';
import { HttpServiceMock } from '../../mocks/http.service.mock';
import { MatchModalComponent } from '../../match-modal/match-modal.component';

const openCases: {reaction: Reaction, response: ReactionResponse, result: boolean}[] = [
    {
        reaction: {id: 1, likes: true},
        response: {likes: true},
        result: true
    },
    {
        reaction: {id: 2, likes: false},
        response: {likes: true},
        result: false
    },
    {
        reaction: {id: 3, likes: false},
        response: {likes: false},
        result: false
    },
    {
        reaction: {id: 4, likes: true},
        response: {likes: false},
        result: false
    },
];

describe('ReactionService', () => {
    let service: ReactionService;
    let httpService: HttpService;
    let dialog: MatDialog;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                {
                    provide: MatDialog,
                    useValue: { open() {} }
                },
                {
                    provide: HttpService,
                    useClass: HttpServiceMock
                }
            ]
        });
        httpService = TestBed.inject(HttpService);
        dialog = TestBed.inject(MatDialog);
    });

    it('should be created', () => {
        service = TestBed.inject(ReactionService);
        expect(service).toBeTruthy();
    });

    openCases.forEach(({reaction, response, result}) => {
        it(`reaction is ${reaction.likes}
         and response is ${response.likes},
          modal should${result ? '' : ' not'} be opened`, (done) => {
            const afterClosedSpy = jasmine.createSpy('afterClosed').and.returnValue(of(null));
            const openSpy = jasmine.createSpy('open').and.returnValue({
                afterClosed: afterClosedSpy
            });

            dialog.open = openSpy;
            service = TestBed.inject(ReactionService);
            spyOn(httpService, 'postReaction').and.returnValue(of(response));

            service.react(reaction.id, reaction.likes).subscribe(() => {
                if (result) {
                    expect(openSpy).toHaveBeenCalledOnceWith(MatchModalComponent);
                    expect(afterClosedSpy).toHaveBeenCalledOnceWith();
                } else {
                    expect(openSpy).not.toHaveBeenCalled();
                    expect(afterClosedSpy).not.toHaveBeenCalled();
                }
                done();
            })
        })
    })

});
