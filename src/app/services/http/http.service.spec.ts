import { TestBed } from '@angular/core/testing';

import { HttpService } from './http.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { User } from '../user/user.service';
import { ReactionResponse, Reaction } from '../reaction/reaction.service';
import { Config } from '../config/config.service';

describe('HttpService', () => {
    let service: HttpService;
    let http: HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [HttpService]
        });
        service = TestBed.inject(HttpService);
        http = TestBed.inject(HttpTestingController);
    });

    afterEach(() => {
        http.verify();
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should get users', () => {
        const requestedUsers: User[] = [
            {
                id: 1,
                name: 'Susan',
                image: '',
                age: 24,
            },
            {
                id: 2,
                name: 'Maria',
                image: '',
                age: 25,
            }
        ];

        service.getUsers().subscribe(users => {
            expect(users).toEqual(requestedUsers);
        });

        const req = http.expectOne('users');
        expect(req.request.method).toEqual('GET');
        req.flush(requestedUsers);
    });

    it('should post reaction and return reaction response', () => {
        const reaction: Reaction = {likes: true, id: 1};
        const reactionResponse: ReactionResponse = {likes: true};
        service.postReaction(reaction).subscribe(r => {
            expect(r).toEqual(reactionResponse);
        });

        const req = http.expectOne('reaction');
        expect(req.request.method).toEqual('POST');
        expect(req.request.body).toEqual(reaction);
        req.flush(reactionResponse);
    });

    it('should get config', () => {
        const config: Config = {url: '.'};
        service.getConfig().subscribe(c => {
            expect(c).toEqual(config);
        })

        const req = http.expectOne('./app.settings.json');
        expect(req.request.method).toEqual('GET');
        req.flush(config);
    });

    it('should get jwt', () => {
        const jwt = {token: ''};
        service.getJWT().subscribe(j => {
            expect(j).toEqual(jwt);
        });

        const req = http.expectOne('jwt');
        expect(req.request.method).toEqual('GET');
        req.flush(jwt);
    })
});
