import { Injectable } from '@angular/core';
import { User } from '../user/user.service';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { ReactionResponse, Reaction } from '../reaction/reaction.service';
import { Config } from '../config/config.service';

@Injectable({
    providedIn: 'root'
})
export class HttpService {

    constructor(private http: HttpClient) { }

    getUsers(): Observable<User[]> {
        return this.http.get<User[]>('users');
    }

    postReaction(reaction: Reaction): Observable<ReactionResponse> {
        return this.http.post<ReactionResponse>('reaction', reaction);
    }

    getJWT(): Observable<{ token: string }> {
        return this.http.get<{token: string}>('jwt');
    }

    getConfig(): Observable<Config> {
        return this.http.get<Config>('./app.settings.json');
    }
}
