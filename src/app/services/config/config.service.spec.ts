import { TestBed } from '@angular/core/testing';

import { ConfigService } from './config.service';
import { HttpService } from '../http/http.service';
import { HttpServiceMock } from '../../mocks/http.service.mock';
import { config, of } from 'rxjs';

describe('ConfigService', () => {
    let service: ConfigService;
    let httpService: HttpService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                {
                    provide: HttpService,
                    useClass: HttpServiceMock
                }
            ]
        });
        service = TestBed.inject(ConfigService);
        httpService = TestBed.inject(HttpService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should get config properly', (done) => {
        const configStub = {url: '.'};
        spyOn(httpService, 'getConfig').and.returnValue(of(configStub));

        service.config$.subscribe(config => {
            expect(config).toEqual(configStub);
            expect(service.config).toEqual(configStub);
            done();
        })
    });

    it('should have empty config in the very beginning', () => {
        expect(service.config).toEqual({url: ''});
    })
});
