import { TestBed } from '@angular/core/testing';

import { ConfigInterceptor } from './config.interceptor';
import { ConfigService } from './config.service';
import { ConfigServiceMock } from '../../mocks/config.service.mock';
import { HttpRequest } from '@angular/common/http';

describe('ConfigInterceptor', () => {
    beforeEach(() => TestBed.configureTestingModule({
        providers: [
            ConfigInterceptor,
            {
                provide: ConfigService,
                useClass: ConfigServiceMock
            }
        ]
    }));

    it('should be created', () => {
        const interceptor: ConfigInterceptor = TestBed.inject(ConfigInterceptor);
        expect(interceptor).toBeTruthy();
    });

    it('should add api endpoint for non static request', () => {
        const configService: ConfigService = TestBed.inject(ConfigService);
        configService.config = {url: 'api-endpoint'};
        const interceptor: ConfigInterceptor = TestBed.inject(ConfigInterceptor);
        const request = new HttpRequest('GET', 'path-to-api');
        const expectedRequest = request.clone({
            url: 'api-endpoint/path-to-api'
        });
        const handler = jasmine.createSpyObj('HttpHandler', ['handle']);

        interceptor.intercept(request, handler);

        expect(handler.handle).toHaveBeenCalledOnceWith(expectedRequest);
    });

    it('should not add api endpoint for static request', () => {
        const configService: ConfigService = TestBed.inject(ConfigService);
        configService.config = {url: 'api-endpoint'};
        const interceptor: ConfigInterceptor = TestBed.inject(ConfigInterceptor);
        const request = new HttpRequest('GET', './static');
        const expectedRequest = request.clone({
            url: './static'
        });
        const handler = jasmine.createSpyObj('HttpHandler', ['handle']);

        interceptor.intercept(request, handler);

        expect(handler.handle).toHaveBeenCalledOnceWith(expectedRequest);
    });
});
