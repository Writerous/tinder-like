import { Injectable } from '@angular/core';
import { Observable, shareReplay, tap } from 'rxjs';
import { HttpService } from '../http/http.service';

export interface Config {
    url: string;
}

@Injectable({
    providedIn: 'root'
})
export class ConfigService {

    constructor(private httpService: HttpService) { }

    config: Config = {url: ''};

    config$: Observable<Config> = this.httpService.getConfig().pipe(
        tap(config => this.config = config),
        shareReplay({refCount: true, bufferSize: 1})
    );
}
