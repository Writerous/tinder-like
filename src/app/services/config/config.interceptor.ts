import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ConfigService } from './config.service';

@Injectable()
export class ConfigInterceptor implements HttpInterceptor {

    constructor(private configService: ConfigService) {}

    intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
        if (ConfigInterceptor.isRequestToStatic(request)) {
            return next.handle(request);
        }
        return next.handle(request.clone({url: `${this.configService.config.url}/${request.url}`}));
    }

    private static isRequestToStatic(request: HttpRequest<unknown>): boolean {
        return request.url.startsWith('./');
    }
}
