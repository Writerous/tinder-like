import { HttpErrorResponse } from '@angular/common/http';
import { ErrorHandler, Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable()
export class ErrorHandlerService implements ErrorHandler {
    constructor(private snackBar: MatSnackBar) {}

    handleError(error?: Error | HttpErrorResponse | { message: string }) {
        let errorMessage = 'unknown error';

        if (error instanceof HttpErrorResponse) {
            errorMessage = `${error.status}, ${error.message}`;
        } else if (typeof error?.message === 'string') {
            errorMessage = error?.message;
        }

        this.snackBar.open(errorMessage, 'Ok');
    }
}
