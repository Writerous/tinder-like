import { TestBed } from '@angular/core/testing';

import { ErrorHandlerService } from './error-handler.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { HttpErrorResponse } from '@angular/common/http';

const errorCases = [
    {
        error: undefined,
        result: 'unknown error'
    },
    {
        error: {message: 'string error'},
        result: 'string error'
    },
    {
        error: new Error('some general error'),
        result: 'some general error'
    },
    {
        error: new TypeError('type error'),
        result: 'type error'
    },
    {
        error: new HttpErrorResponse({error: new Error('not found'), status: 404, statusText: 'Not Found', url: './url'}),
        result: '404, Http failure response for ./url: 404 Not Found'
    }
]

describe('ErrorHandlerService', () => {
    let service: ErrorHandlerService;
    let snackBar: MatSnackBar;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                {
                    provide: MatSnackBar,
                    useValue: jasmine.createSpyObj('MatSnackBar',['open']),
                },
                ErrorHandlerService
            ]
        });
        service = TestBed.inject(ErrorHandlerService);
        snackBar = TestBed.inject(MatSnackBar);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    errorCases.forEach(errorCase => {
        it(`should handle error ${errorCase.error?.message ?? 'undefined'}
         and return message ${errorCase?.result} `, () => {
            service.handleError(errorCase?.error);
            expect(snackBar.open).toHaveBeenCalledOnceWith(errorCase.result, 'Ok');
        })
    })

});
