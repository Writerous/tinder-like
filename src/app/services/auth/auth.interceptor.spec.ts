import { TestBed } from '@angular/core/testing';

import { AuthInterceptor } from './auth.interceptor';
import { LoginService } from '../login/login.service';
import { LoginServiceMock } from '../../mocks/login.service.mock';
import { HttpHandler, HttpHeaders, HttpRequest } from '@angular/common/http';

describe('AuthInterceptor', () => {
    beforeEach(() => TestBed.configureTestingModule({
        providers: [
            AuthInterceptor,
            {
                provide: LoginService,
                useClass: LoginServiceMock
            }
        ]
    }));

    it('should be created', () => {
        const interceptor: AuthInterceptor = TestBed.inject(AuthInterceptor);
        expect(interceptor).toBeTruthy();
    });

    it('should add auth header', () => {
        const loginService: LoginService = TestBed.inject(LoginService);

        const jwtSpy = spyOnProperty(loginService, 'jwt').and.returnValue('token');

        const interceptor: AuthInterceptor = TestBed.inject(AuthInterceptor);
        const request = new HttpRequest('GET', './url');
        const expectedRequest = request.clone({
            setHeaders: { Authorization: 'Bearer token'}
        });
        const handler = jasmine.createSpyObj('HttpHandler', ['handle']);

        interceptor.intercept(request, handler);

        expect(handler.handle).toHaveBeenCalledOnceWith(expectedRequest);
        expect(jwtSpy).toHaveBeenCalledOnceWith();
    })
});
