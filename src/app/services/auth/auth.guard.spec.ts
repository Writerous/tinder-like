import { TestBed } from '@angular/core/testing';

import { AuthGuard } from './auth.guard';
import { LoginService } from '../login/login.service';
import { LoginServiceMock } from '../../mocks/login.service.mock';
import { of } from 'rxjs';

describe('AuthGuard', () => {
    let guard: AuthGuard;
    let loginService: LoginService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                {
                    provide: LoginService,
                    useClass: LoginServiceMock
                }
            ]
        });
        loginService = TestBed.inject(LoginService);
    });

    it('should be created', () => {
        guard = TestBed.inject(AuthGuard);
        expect(guard).toBeTruthy();
    });

    it('should grant access when jwt is truthy', (done) => {
        loginService.jwt$ = of('token');
        guard = TestBed.inject(AuthGuard);

        guard.canActivate().subscribe(v => {
            expect(v).toBe(true);
            done();
        });
    });

    it('should deny access when jwt is falsy', (done) => {
        loginService.jwt$ = of(null);
        guard = TestBed.inject(AuthGuard);

        guard.canActivate().subscribe(v => {
            expect(v).toBe(false);
            done();
        })
    })
});
