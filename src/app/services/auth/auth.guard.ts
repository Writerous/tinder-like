import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { map, Observable } from 'rxjs';
import { LoginService } from '../login/login.service';

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate {

    constructor(private loginService: LoginService) {}

    canActivate(): Observable<boolean> {
        return this.loginService.jwt$.pipe(map(Boolean))
    }
}
