import { ErrorHandler, Injectable, OnDestroy } from '@angular/core';
import { ConfigService } from '../config/config.service';
import { BehaviorSubject, concatMap, map, Observable, scan, shareReplay } from 'rxjs';
import { HttpService } from '../http/http.service';

export interface User {
    id: number;
    name: string;
    age: number;
    image: string;
}

@Injectable()
export class UserService implements OnDestroy {

    constructor(private configService: ConfigService, private httpService: HttpService, private errorHandler: ErrorHandler) { }

    users$ = this.httpService.getUsers().pipe(
        map(users => users.map(u => ({...u, image: this.configService.config.url + '/' + u.image}))),
        shareReplay({refCount: true, bufferSize: 1})
    )

    user$: Observable<User | null> = this.users$.pipe(
        concatMap(users => {
            return this.userClickSubject.pipe(
                scan(acc => {
                    if (acc === users.length) {
                        this.errorHandler.handleError(new Error('Users are over, return back later)'));
                        return 1;
                    }

                    return acc + 1;
                }, 0),
                map(counter => users[counter - 1]))
        })
    );

    private userClickSubject = new BehaviorSubject(null);

    nextUser(): void {
        this.userClickSubject.next(null);
    }

    ngOnDestroy() {
        this.userClickSubject.complete();
    }
}
