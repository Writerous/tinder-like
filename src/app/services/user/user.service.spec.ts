import { TestBed } from '@angular/core/testing';

import { User, UserService } from './user.service';
import { ConfigService } from '../config/config.service';
import { ConfigServiceMock } from '../../mocks/config.service.mock';
import { ErrorHandler } from '@angular/core';
import { ErrorHandlerServiceMock } from '../../mocks/error-handler.service.mock';
import { of } from 'rxjs';
import { HttpService } from '../http/http.service';
import { HttpServiceMock } from '../../mocks/http.service.mock';

const requestedUsers: User[] = [
    {
        id: 1,
        name: 'Susan',
        image: '',
        age: 24,
    },
    {
        id: 2,
        name: 'Maria',
        image: '',
        age: 25,
    },
    {
        id: 3,
        name: 'Cristie',
        image: '',
        age: 26,
    }
];

describe('UserService', () => {
    let service: UserService;
    let configService: ConfigService;
    let httpService: HttpService;
    let errorHandler: ErrorHandler;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                {
                    provide: ConfigService,
                    useClass: ConfigServiceMock
                },
                {
                    provide: ErrorHandler,
                    useClass: ErrorHandlerServiceMock
                },
                {
                    provide: HttpService,
                    useClass: HttpServiceMock
                },
                UserService
            ]
        });
        configService = TestBed.inject(ConfigService);
        httpService = TestBed.inject(HttpService);
        errorHandler = TestBed.inject(ErrorHandler);
    });

    it('should be created', () => {
        service = TestBed.inject(UserService);
        expect(service).toBeTruthy();
    });

    it('should show first user without click', (done) => {
        spyOn(httpService, 'getUsers').and.returnValue(of(requestedUsers));

        service = TestBed.inject(UserService);

        service.user$.subscribe(user => {
            expect(user).toEqual({ ...requestedUsers[0], image: './' + requestedUsers[0].image });
            done();
        })
    });

    it('should show next user every time user clicks, (10 clicks)', (done) => {
        spyOn(httpService, 'getUsers').and.returnValue(of(requestedUsers));

        service = TestBed.inject(UserService);

        let counter = 0;
        const currentUser = (): User => {
            const u = requestedUsers[counter % requestedUsers.length];
            return ({ ...u, image: './' + u.image })
        }

        service.user$.subscribe(user => {
            expect(user).toEqual(currentUser());
            if (counter === 10) {
                done();
            }
        })

        do {
            counter++;
            service.nextUser();
        } while (counter < 10)
    });

    it('should handler error when users are over (3 times), (10 clicks)', (done) => {
        spyOn(httpService, 'getUsers').and.returnValue(of(requestedUsers));
        spyOn(errorHandler, 'handleError');

        service = TestBed.inject(UserService);

        let counter = 0;

        service.user$.subscribe(user => {
            if (counter === 10) {
                // users ending occurs 10 / 3 = 3 times
                expect(errorHandler.handleError)
                    .toHaveBeenCalledTimes(Math.floor(counter / requestedUsers.length));
                done();
            }
        })

        do {
            counter++;
            service.nextUser();
        } while (counter < 10)
    })

});
