import { Inject, Injectable, InjectionToken } from '@angular/core';
import { defer, iif, map, of, shareReplay, tap } from 'rxjs';
import { HttpService } from '../http/http.service';

export const LOCAL_STORAGE = new InjectionToken<Storage>('local_storage', {
    providedIn: 'root',
    factory: () => window.localStorage
})

@Injectable({
    providedIn: 'root'
})
export class LoginService {

    constructor(private httpService: HttpService, @Inject(LOCAL_STORAGE) private localStorage: Storage) { }

    jwtRequest$ = this.httpService.getJWT().pipe(
        map(({token}) => token),
        tap(jwt => this.localStorage.setItem('jwt', jwt)),
        shareReplay({refCount: true, bufferSize: 1})
    );

    jwt$ = iif(() => !!this.jwt, defer(() => of(this.jwt)), this.jwtRequest$);

    get jwt(): string | null {
        return this.localStorage.getItem('jwt');
    }
}
