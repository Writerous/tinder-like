import { TestBed } from '@angular/core/testing';
import { LOCAL_STORAGE, LoginService } from './login.service';
import { HttpService } from '../http/http.service';
import { HttpServiceMock } from '../../mocks/http.service.mock';
import { of } from 'rxjs';

class LocalStorageStub implements Storage {
    private map = new Map<string, string>();

    setItem(key: string, value: string): void {
        this.map.set(key, value);
    };
    getItem(key: string): string | null {
        return this.map.get(key) ?? null;
    }

    get length(): number {
        return this.map.size;
    }

    clear() {
        this.map.clear();
    }

    removeItem(key: string) {
        this.map.delete(key);
    }

    key(index: number): string | null {
        return [...this.map.keys()][index];
    }
}

describe('LoginService', () => {
    let service: LoginService;
    let localStorage: Storage;
    let httpService: HttpService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                {
                    provide: LOCAL_STORAGE,
                    useClass: LocalStorageStub,
                },
                {
                    provide: HttpService,
                    useClass: HttpServiceMock
                }
            ]
        });

        localStorage = TestBed.inject(LOCAL_STORAGE);
        httpService = TestBed.inject(HttpService);
    });

    it('should be created', () => {
        service = TestBed.inject(LoginService);
        expect(service).toBeTruthy();
    });

    it('should get jwt properly and write in localStorage', (done) => {
        spyOn(localStorage, 'setItem').and.callThrough();
        spyOn(localStorage, 'getItem').and.callThrough();
        spyOn(httpService, 'getJWT').and.returnValue(of({token: 'token'}));

        service = TestBed.inject(LoginService);

        service.jwtRequest$.subscribe(token => {
            expect(token).toEqual('token');
            expect(service.jwt).toEqual('token');
            expect(localStorage.getItem).toHaveBeenCalledOnceWith('jwt');
            expect(localStorage.setItem).toHaveBeenCalledOnceWith('jwt', 'token');
            done();
        })
    });

    it('should have null token in before request', () => {
        spyOn(localStorage, 'getItem').and.callThrough();

        service = TestBed.inject(LoginService);

        expect(service.jwt).toEqual(null);
        expect(localStorage.getItem).toHaveBeenCalledOnceWith('jwt');
    });

    it('should get token local storage if it exists', (done) => {
        spyOn(httpService, 'getJWT').and.returnValue(of({token: 'token from request'}));
        spyOn(localStorage, 'getItem').and.returnValue('token from storage');

        service = TestBed.inject(LoginService);
        service.jwt$.subscribe(token => {
            expect(token).toEqual('token from storage');
            done();
        })
    })
});
