import { APP_INITIALIZER, ErrorHandler, NgModule, Provider } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { ConfigService } from './services/config/config.service';
import { firstValueFrom } from 'rxjs';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar';
import { ViewerComponent } from './viewer/viewer.component';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { ConfigInterceptor } from './services/config/config.interceptor';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './services/auth/auth.guard';
import { AuthInterceptor } from './services/auth/auth.interceptor';
import { MatDialogModule } from '@angular/material/dialog';
import { MatchModalComponent } from './match-modal/match-modal.component';
import { ErrorHandlerService } from './services/error/error-handler.service';
import { MatSnackBarModule } from '@angular/material/snack-bar';

const routes: Routes = [
    {
        path: '',
        component: ViewerComponent,
        canActivate: [AuthGuard]
    },
    {
        path: '**',
        redirectTo: '/'
    }
];

const interceptors: Provider[] = [
    {
        provide: HTTP_INTERCEPTORS,
        useClass: ConfigInterceptor,
        multi: true
    },
    {
        provide: HTTP_INTERCEPTORS,
        useClass: AuthInterceptor,
        multi: true
    }
];

const matModules = [
    MatSnackBarModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatDialogModule,
];

@NgModule({
    declarations: [
        AppComponent,
        ViewerComponent,
        MatchModalComponent
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        BrowserAnimationsModule,
        matModules,
        RouterModule.forRoot(routes)
    ],
    providers: [
        {
            provide: ErrorHandler,
            useClass: ErrorHandlerService
        },
        {
            provide: APP_INITIALIZER,
            deps: [ConfigService],
            useFactory: (configService: ConfigService) => () => firstValueFrom(configService.config$),
            multi: true
        },
        interceptors
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
