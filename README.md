# TinderLike

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.3.7.

Node is 16.10.0

Npm is 7.24.0

If you have some problems with running app, please install versions above.

## Development server (Client app)

Run `npm run serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Development server (Backend app)

Run `npm run start-server` for a dev backend mock-server (implemented with Express). Mock server listens request on `http://localhost:5000`

## Build (Client app)

Run `npm run build:prod` to build the project for production purpose. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `npm run test` to execute the unit tests via [Karma](https://karma-runner.github.io).
Check coverage in console.

## Code coverage

You can check current code coverage the following way.
- Run `npm run start-server` 
- Navigate http://localhost:5000/coverage

## Backend contracts

### /users

- method: **GET**
- response: ``{ id: number; name: string; age: number;image: string; }[]`` (represents a portion of users )

### /reaction

- method: **POST**
- body: ``{ id: number; likes: boolean; }`` (represents your reaction about other user with specified id (like, dislike))
- response: ``{ likes?: boolean; }`` (represents possible reaction from user you liked or disliked)

### /jwt

- method: **GET**
- response ``{ token: string; }`` (represents jwt token for authorized request, backend identify user and his actions only by token)

### ./app.settings.json
it is request to static folder to load environment sensitive data (e.g. backend url)

- method: **GET**
- response ``{ ur: string; }`` (represents app.seettings.json file, you can see dev file in repo)
